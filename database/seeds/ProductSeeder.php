<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Products;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$Category = Category::first(); 
        factory(Products::class,3)->create(
        	[
        		'category_id'=>$Category->id,
        	]
        );
    }
}
