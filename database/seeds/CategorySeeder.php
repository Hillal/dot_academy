<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Category::class,3)->create([
            'product_count' => '0'
        ]);
        // Category::created{['name_category' => 'Category1']};
    }
}
