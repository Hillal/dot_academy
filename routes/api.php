<?php

use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use App\Products;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//membuat routing API dengan prefix "localhost/api/v1/(....)"
Route::prefix('v1')->group(function(){
	Route::prefix('belajar')->group(function(){
		Route::get('cobaget','API\V1\CobaController@method_get');
		Route::post('cobapost','API\V1\CobaController@method_post');
		Route::post('trycatch','API\V1\CobaController@trycatch');
	});

	Route::prefix('kategori')->group(function(){
		Route::get('/','API\V1\KategoriController@index');
		Route::get('/{id}','API\V1\KategoriController@detail');
		Route::post('/store','API\V1\KategoriController@store');
	});

	Route::prefix('customer')->group(function(){
		Route::get('/','API\V1\CustomerController@index');
		Route::get('/{id}','API\V1\CustomerController@detail');
		Route::post('/store','API\V1\CustomerController@store');
	});
});





//register
Route::post('register', 'API\Auth\RegisterController@register');
//login
Route::post('login', 'API\Auth\LoginController@login');


// success login
Route::group(['middleware' => 'auth:api'], function () {

    Route::apiResource('product','API\V1\ProductController');
	Route::apiResource('category','API\V1\CategoryController');
	Route::apiResource('customers','API\V1\CustomersController');

	// user detail
    Route::get('/user/detail', 'API\V1\UserController@detail');
    //order
    Route::post('store', 'API\V1\OrderController@store');
    Route::get('show/{id}', 'API\V1\OrderController@show');
    //order detail
    Route::post('order_detail', 'API\V1\OrderDetailController@store');
    Route::get('order_detail/{id}', 'API\V1\OrderDetailController@show');
 });



// Route::get('/product/{id}', function(Products $id){
// 	return new ProductResource($id);
// });



// Route::get('/product', function(){
// 	return ProductResource::collection(Products::all());
// });

// Route::get('/product', function() {
//     return ProductCollection::collection(Products::all());
// });
