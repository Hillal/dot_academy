<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth/login');
});

// Route::get('/product', function () {
//     return view('product/template');
// });

//login
Route::get("/login", "LoginController@showLogin")->name("login"); // show login form
Route::post("/login", "LoginController@doLogin")->name("login.doLogin");

//logout
Route::get("/logout", "LoginController@logout")->name("logout");

Route::middleware("auth")->group(function() {

//produk
	Route::prefix('product')->group(function(){
		Route::post("/store", "ProductController@store");
		Route::get('/{id}', 'ProductController@detail');
		Route::get('/{id}/delete', 'ProductController@delete');
		Route::get('/{id}/edit', 'ProductController@edit');
		Route::post('/{id}/update', 'ProductController@update');
		Route::get("/", "ProductController@index");
		Route::get("/nama", "ProductController@nama");

	});


//customer
	Route::prefix('customers')->group(function(){
		Route::post("/tambah", "CustomersController@tambah");
		Route::get('/{id}', 'CustomersController@detail');
		Route::get('/{id}/edit', 'CustomersController@edit');
		Route::post('/{id}/update', 'CustomersController@update');
		Route::get('/{id}/delete', 'CustomersController@delete');
		Route::get("/", "CustomersController@index");

	});

//kategor
// Route::get("/category", "CategoryController@index");
	Route::prefix('category')->group(function(){
		Route::get("/", "CategoryController@index")->name('category.index');
		Route::post("/store", "CategoryController@store")->name('category.store');
		Route::get('/{id}', 'CategoryController@show');
		Route::get('/{id}/delete', 'CategoryController@delete');
		Route::get('/{id}/edit', 'CategoryController@edit');
		Route::post('/{id}/update', 'CategoryController@update');
	});

//order
	Route::prefix('orders')->group(function(){
		Route::get("/", "OrderController@index");
		Route::post("/", "OrderController@store");
		Route::get('/detail/{id}', 'OrderController@detail');
		Route::post('/detail/{id}/add', 'OrderController@addProduct')->name('order.add');
		Route::get('/{id}/destroy', 'OrderController@destroy');
		Route::get('/detail/{id}/delete', 'OrderController@delete');

	});

});