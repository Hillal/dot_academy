  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Order
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Order</h3>
            </div>

            @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
            @endif
            <form role="form" action="/orders/{{$dataOrderDetail->id}}/update" method="post">

              @csrf
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <td>Nama Produk</td>
                  <td>:</td>
                  <td><a type="text" name="nama" value="{{ $dataOrderDetail->product }->}"></td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>:</td>
                  <td><a type="text" name="unit_price" value="{{ $dataOrder->email }}"></td>
                </tr>
                <tr>
                  <td>Jumlah Barang</td>
                  <td>:</td>
                  <td><input type="text" name="password" value="{{ $dataCustomers->password }}"></td>
                </tr>
              </table>

              <input class="btn btn-primary" type="submit" value="update"></input>
              <a class="btn btn-warning" href="/customers">Back</a>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')