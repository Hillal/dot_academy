 @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Category
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Kategori</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">

              @csrf

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Katagori</label>
                  <input type="text" name="name_category" class="form-control" placeholder="Masukkan nama kategori">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Kategori</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="/category" method="GET">
                  <span class="pull-right">
                    <input type="text" name="search" class="form-control" placeholder="Search here...">
                  </span>
                </form>
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  <th>Nama Kategori</th>
                  <th>Jumlah Produk</th>
                  <th>Action</th>
                </tr>
                @foreach($Category as $item)
                <tr>
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->name_category }}</td>
                  <td>{{ $item->product_count}}</td>
                  <td>
                  	<a class="btn btn-primary btn-sm" href="/category/{{$item->id}}">Detail</a>
                    <a class="btn btn-success btn-sm" href="/category/{{$item->id}}/edit">Edit</a>
                    <a class="btn btn-danger btn-sm" href="/category/{{$item->id}}/delete">Delete</a>
                  </td>
                </tr>                  
                @endforeach
              </table>
            </div>

            <div class="text-center">
              {!! $Category->appends(request()->all())->links() !!}
            </div>

          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
