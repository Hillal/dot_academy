@include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <!-- Nama kategori -->
        Kategori: {{ $Category->name_category }}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Produk dalam kategori ini</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>ID Produk</th>
                    <th>Nama Produk</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($Category->products as $product)
                    <tr>
                      <td>{{ $product->id }}</td>
                      <td>{{ $product->nama }}</td>
                    </tr>
                  @empty
                    <tr>
                      <td colspan="2" align="center">
                        Belum ada produk dalam kategori ini.
                      </td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')
