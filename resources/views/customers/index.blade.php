  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Customers
      </h1>
    </section>



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Customers</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
            @endif
            <form role="form" action="/customers/tambah" method="post" enctype="multipart/form-data">

              @csrf

              <div class="box-body">

                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                  </div>
                @endif

                <div class="form-group">
                  <label for="exampleInputEmail1">Fist Nama</label>
                  <input type="text" name="first_name" class="form-control" placeholder="Masukkan nama anda">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Last Nama</label>
                  <input type="text" name="name" class="form-control" placeholder="Masukkan nama anda">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="text" name="email" class="form-control" placeholder="Masukkan email">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="text" name="password" class="form-control" placeholder="Masukkan password">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">No. Telp.</label>
                  <input type="text" name="phone_number" class="form-control" placeholder="Masukkan nomor telepon">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Alamat</label>
                  <input type="text" name="address" class="form-control" placeholder="Masukkan alamat">
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="/customers" method="GET">
                  <span class="pull-right">
                    <input type="text" name="search" class="form-control" placeholder="Search here...">
                  </span>
                </form>
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>No. Telp.</th>
                  <th>Alamat</th>
                  <th>Action</th>
                </tr>
                @foreach($dataCustomers as $item)
                <tr>
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->first_name }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->email }}</td>
                  <td>{{ $item->password }}</td>
                  <td>{{ $item->phone_number }}</td>
                  <td>{{ $item->address }}</td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="/customers/{{$item->id}}">Detail</a>
                    <a class="btn btn-success btn-sm" href="/customers/{{$item->id}}/edit">Edit</a>
                    <a class="btn btn-danger btn-sm" href="/customers/{{$item->id}}/delete">Delete</a>
                  </td>

                  <td></td>
                </tr>                  
                @endforeach
              </table>
            </div>
            <div class="text-center">
              {!! $dataCustomers->appends(request()->all())->links() !!}
            </div>

          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')