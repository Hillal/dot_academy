  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customers
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Customers</h3>
            </div>

            @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
            @endif
            <form role="form" action="/customers/{{$dataCustomers->id}}/update" method="post">

              @csrf
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <td>Id</td>
                  <td>:</td>
                  <td><input type="text" name="id" value="{{ $dataCustomers->id }}" readonly="true"></td>
                </tr>
                <tr>
                  <td>Nama</td>
                  <td>:</td>
                  <td><input type="text" name="name" value="{{ $dataCustomers->name }}"></td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td><input type="text" name="email" value="{{ $dataCustomers->email }}"></td>
                </tr>
                <tr>
                  <td>Password</td>
                  <td>:</td>
                  <td><input type="text" name="password" value="{{ $dataCustomers->password }}"></td>
                </tr>
                <tr>
                  <td>No. Telp.</td>
                  <td>:</td>
                  <td><input type="text" name="phone_number" value="{{ $dataCustomers->phone_number }}"></td>
                </tr>
                <tr>
                  <td>Address</td>
                  <td>:</td>
                  <td><input type="text" name="address" value="{{ $dataCustomers->address }}"></td>
                </tr>
              </table>

              <input class="btn btn-primary" type="submit" value="update"></input>
              <a class="btn btn-warning" href="/customers">Back</a>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')