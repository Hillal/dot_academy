  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Product
      </h1>
    </section>



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Product</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
            @endif
            <form role="form" action="/product/store" method="post" enctype="multipart/form-data">


              @csrf

              <div class="box-body">

                

                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                  </div>
                @endif


                <div class="form-group">
                  <label for="exampleInputEmail1">Product Name</label>
                  <input type="text" name="nama" class="form-control" placeholder="Masukkan nama produk">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Category</label>
                  <select class="form-control" name="category_id" required>
                    <option>-- Pilih Salah Satu --</option>
                    @foreach($Category as $category)
                      <option value="{{ $category->id }}">{{ $category->name_category }}</option>
                    @endforeach
                  </select>
                  <!-- <input type="text" name="category" class="form-control" placeholder="Masukkan nama kategori produk"> -->
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Harga</label>
                  <input type="text" name="unit_price" class="form-control" placeholder="Masukkan harga produk">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Foto</label>
                  <input type="file" name="image" class="form-control" placeholder="Masukkan foto">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="/product" method="GET">
                  <span class="pull-right">
                    <input type="text" name="search" class="form-control" placeholder="Search here...">
                  </span>
                </form>
              <table class="table table-bordered">
                <tr>
                  <th>Id</th>
                  <th>Nama Product</th>
                  <th>Kategori</th>
                  <th>Harga</th>
                  <th>Foto</th>
                  <th>Action</th>
                </tr>
                @foreach($dataProduct as $item)
                <tr>
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->nama }}</td>
                  <td>{{ $item->category->name_category }}</td>
                  <td>{{ $item->unit_price }}</td>
                  <td><img src="/images/{{ $item->image }}" style="width: 50px; height: 40px"></td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="/product/{{$item->id}}">Detail</a>
                    <a class="btn btn-success btn-sm" href="/product/{{$item->id}}/edit">Edit</a>
                    <a class="btn btn-danger btn-sm" href="/product/{{$item->id}}/delete">Delete</a>
                  </td>

                  <td></td>
                </tr>                  
                @endforeach
              </table>
            </div>

            <div class="text-center">
              {!! $dataProduct->appends(request()->all())->links() !!}
            </div>

          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')