  @include('base.header')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Product
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Product</h3>
            </div>

            @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
            @endif
            <form role="form" action="/product/{{$dataProduct->id}}/update" method="post" enctype="multipart/form-data">

              @csrf
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <td>Id</td>
                  <td>:</td>
                  <td><input type="text" name="id" value="{{ $dataProduct->id }}" readonly="true"></td>
                </tr>
                <tr>
                  <td>Nama</td>
                  <td>:</td>
                  <td><input type="text" name="nama" value="{{ $dataProduct->nama }}"></td>
                </tr>
                <tr>
                  <td>Kategori</td>
                  <td>:</td>

                  <td>
                    <select class="form-control" name="category_id" required>
                      <option >Pilih</option>
                         @foreach($Category as $category)
                     <option value="{{$category->id}}" {{($dataProduct->category_id == $category->id)?'selected':''}}>{{$category->name_category}}</option>
                     @endforeach
                      </option>
                    </select>
                  </td>
                  <!-- <td><input type="text" name="category" value="{{ $dataProduct->category }}"></td> -->
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>:</td>
                  <td><input type="text" name="unit_price" value="{{ $dataProduct->unit_price }}"></td>
                </tr>

                <tr>
                  <td>Foto</td>
                  <td>:</td>
                  <td>
                    <img src="/images/{{ $dataProduct->image }}" style="width: 50px; height: 40px">
                    <input type="file" name="image">
                  </td>
                </tr>
              </table>

              <input class="btn btn-primary" type="submit" value="update"></input>
              <a class="btn btn-warning" href="/product">Back</a>
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('base.footer')