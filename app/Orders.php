<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
	use SoftDeletes;
    protected $table = 'orders';

    protected $fillable = ['customer_id', 'total'];

    public function Customers() {
    	return $this->hasOne(Customers::class, "id", "customer_id");
    }

    public function Orderdetail()
    {
    	return $this->hasMany(OrderDetail::class, "order_id", "id");
    }

    public function delete()
    {
    	$this->OrderDetail()->delete();
        return parent::delete();
    }
}
