<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    protected $table = 'products';

    protected $fillable = ["nama", "category", "unit_price"];

    use SoftDeletes;

    public function category()
    {
    	return $this->hasOne(Category::class, "id", "category_id")->withTrashed();
    }
}
