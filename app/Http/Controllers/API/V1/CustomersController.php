<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Customers;
use Exception;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $dataCustomers = customers::all();

            $code=200;
            $response=$dataCustomers;

        }catch(Exception $e){
            $code = 500;
            $response = $e->getMessage();   
        return apiResponseBuilder($code,$response);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request,[
                'first_name'=>'required',
                'name'=>'required',
                'password'=>'required',
                'email'=>'required',
                'phone_number'=>'required',
                'address'=>'required',
            ]);

            $dataCustomers = new Customers;
            
            $dataCustomers->first_name = $request->first_name;
            $dataCustomers->name = $request->name;
            $dataCustomers->email = $request->email;
            $dataCustomers->password = $request->password;
            $dataCustomers->phone_number = $request->phone_number;
            $dataCustomers->address = $request->address;
            
            $dataCustomers->save();
            $response=$dataCustomers;
            $code=200;

        }catch(Exception $e){
            if($e instanceof ValidationException){
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();  
            } 
            
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $dataCustomers= Customers::findOrFail($id);

            $code=200;
            $response=$dataCustomers;

        }catch(Exception $e){
            if($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();  
            }
            // $response = $e->getMessage();   
            // $code = 500;
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $dataCustomers= Customers::find($id);

            $dataCustomers->first_name = $request->first_name;
            $dataCustomers->name = $request->name;
            $dataCustomers->email = $request->email;
            $dataCustomers->password = $request->password;
            $dataCustomers->phone_number = $request->phone_number;
            $dataCustomers->address = $request->address;
            $dataCustomers->save();

            $code=200;
            $response = $dataCustomers;
        }catch(Exception $e){   
            $code = 500;
            $response = $e->getMessage();  
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try{
            $dataCustomers= Customers::find($id);
            $dataCustomers->delete();
            $code=200;
            $response=$dataCustomers;
        }catch(Exception $e){
            $code = 500;
            $response = $e->getMessage();  
        }
        return apiResponseBuilder($code,$response);
    }
}
