<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Products;
use App\Category;
use Exception;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $dataProduct = ProductResource::collection(Products::all());

            $response=$dataProduct;
            $code=200;
        } catch (Exception $e) {
            $code = 500;
            $response = $e->getMessage() ;
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try{
            $this->validate($request,[
                'nama' => 'required | min: 3',
                'category_id' => 'required',
                'unit_price' => 'required | numeric',
                'image' => 'required |image|mimes:jpg,jpeg,png,gif',
            ]);

            $dataProduct = new Products();
            if (!$dataProduct) throw new Exception("Error Processing Request", 1);
            if (!isset($request->nama)) throw new Exception("Data Harus Diisi", 1);
            

            Category::where('id', $request->category_id)->increment('product_count');
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
            
            
            $dataProduct->nama = $request->nama;
            $dataProduct->category_id = $request->category_id;
            $dataProduct->unit_price = $request->unit_price;
            $dataProduct->image = $imageName;
            
            $dataProduct->save();
            $code=200;
            $response= new ProductResource($dataProduct);
           

        }catch(Exception $e){
            if($e instanceof ValidationException){
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();  
            } 
            
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $dataProduct= new ProductResource(Products::findOrFail($id));

            $code=200;
            $response=$dataProduct;

        }catch(Exception $e){
            if($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();  
            }
            // $response = $e->getMessage();   
            // $code = 500;
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
         'nama'          => 'required|string',
         'category_id'      => 'required|string',
         'unit_price'    => 'required|numeric',
         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

         ]);
            $dataProduct = Products::find($id);

            if (!$dataProduct) throw new Exception("Error Processing Request", 1);//data yg dimasukkan bukan dataProduct
            if (!isset($request->name_category)) throw new Exception("data harus isi", 1);//validasi kategori
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'),$imageName);
            
            $dataProduct->nama = $request->nama;
            $dataProduct->category_id = $request->category_id;
            $dataProduct->unit_price = $request->unit_price;
            $dataProduct->image = $imageName;
            $data->save();

            $code = 200;
            $response = new ProductResource($dataProduct); 
            
        } catch (Exception $e) {
            // $code = 500;
            // $response = $e->getMessage();
            if ($e instanceof ValidationException) {
               $code = 400;
               $response = 'data tidak ada';
            }else{
                $code = 500;
                $response = $e->getMessage(); 
            } 
        }
        return apiResponseBuilder($code,$response);
        //  try{
        //     $dataProduct= new ProductResource(Products::find($id));

        //     $imageName = time().'.'.request()->image->getClientOriginalExtension();
        //     request()->image->move(public_path('images'), $imageName);
            
            
        //     $dataProduct->nama = $request->nama;
        //     $dataProduct->category_id = $request->category_id;
        //     $dataProduct->unit_price = $request->unit_price;
        //     $dataProduct->image = $imageName;
        //     $Category->save();

        //     $code=200;
        //     $response = $dataProduct;
        // }catch(Exception $e){   
        //     $code = 500;
        //     $response = $e->getMessage();  
        // }
        // return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $dataProduct= new ProductResource(Products::find($id));
            $dataProduct->delete();
            $code=200;
            $response=$dataProduct;
        }catch(Exception $e){
            $code = 500;
            $response = $e->getMessage();  
        }
        return apiResponseBuilder($code,$response);
        
    }
}
