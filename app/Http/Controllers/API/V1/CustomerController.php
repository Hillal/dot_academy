<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customers;
use Exception;

/**
* 
*/
class CustomerController extends Controller
{
	
	public function index()
	{
		try{
			$dataCustomers = customers::all();

			return apiResponseBuilder(200,$dataCustomers);

		}catch(Exception $e){
			$response = $e->getMessage();	
			$code = 500;
		return apiResponseBuilder(500,$e->getMessage());
		}
			
		
	}

	public function detail($id)
	{
		try{
			$dataCustomers= Customers::find($id);
			return apiResponseBuilder(200,$dataCustomers);

		}catch(Exception $e){
			$response = $e->getMessage();	
			$code = 500;
		return apiResponseBuilder(500,$e->getMessage());
		}
	}

	public function store(Request $request)
	{
		try{
			$dataCustomers = new Customers;
        	
			$dataCustomers->first_name = $request->first_name;
	   		$dataCustomers->name = $request->name;
	   		$dataCustomers->email = $request->email;
	   		$dataCustomers->password = $request->password;
	   		$dataCustomers->phone_number = $request->phone_number;
	   		$dataCustomers->address = $request->address;

			$dataCustomers->save();
			$response=$dataCustomers;
			$code=200;

			return apiResponseBuilder(200,$dataCustomers);
		}catch(Exception $e){
			$response = $e->getMessage();	
			$code = 500;
		return apiResponseBuilder(500,$e->getMessage());
		}
	}
}
?>