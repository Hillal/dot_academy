<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function detail()
    {
        $user = Auth::user();
        return apiResponseSuccess('Detail user login', $user, 200);
    }
}
