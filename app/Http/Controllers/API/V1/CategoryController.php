<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Category;
use Exception;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $Category = category::all();

            $response=$Category;
            $code=200;

        }catch(Exception $e){

            // if($e instanceof ModeNotFoundExeption){
            //     $response = 'not found';   
            //     $code = 404;
            // }else if($e instanceof ValidationExeption){
            //     $code = 405;
            //     $response = 'method salah';   
                
            // }
            $code = 500;
            $response = $e->getMessage() ;   
        
        
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request,[
                'name_category'=>'required',
            ]);

            $Category = new Category;
            
            
            $Category->name_category = $request->name_category;
            $Category->product_count=0;
            
            $Category->save();
            $response=$Category;
            $code=200;

        }catch(Exception $e){
            if($e instanceof ValidationException){
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();  
            } 
            
        }
        return apiResponseBuilder($code,$response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Category= Category::findOrFail($id);

            $code=200;
            $response=$Category;

        }catch(Exception $e){
            if($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();  
            }
            // $response = $e->getMessage();   
            // $code = 500;
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $Category= Category::find($id);

            $Category->name_category = $request->name_category;
            $Category->save();

            $code=200;
            $response = $Category;
        }catch(Exception $e){   
            $code = 500;
            $response = $e->getMessage();  
        }
        return apiResponseBuilder($code,$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $Category= Category::find($id);
            $Category->delete();
            $code=200;
            $response=$Category;
        }catch(Exception $e){
            $code = 500;
            $response = $e->getMessage();  
        }
        return apiResponseBuilder($code,$response);
    }
}
