<?php 

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Exception;

class KategoriController extends Controller
{
	public function index()
	{
		try{
			$Category = category::all();

			return apiResponseBuilder(200,$Category);

		}catch(Exception $e){
			$response = $e->getMessage();	
			$code = 500;
		return apiResponseBuilder(500,$e->getMessage());
		}
			
		
	}

	public function detail($id)
	{
		try{
			$Category= Category::find($id);
			return apiResponseBuilder(200,$Category);

		}catch(Exception $e){
			$response = $e->getMessage();	
			$code = 500;
		return apiResponseBuilder(500,$e->getMessage());
		}
	}

	public function store(Request $request)
	{
		try{
			$Category = new Category;
        	
			$Category->name_category = $request->name_category;
			$Category->product_count=0;
			
			$Category->save();
			$response=$Category;
			$code=200;

			return apiResponseBuilder(200,$Category);
		}catch(Exception $e){
			$response = $e->getMessage();	
			$code = 500;
		return apiResponseBuilder(500,$e->getMessage());
		}
	}
}

?>