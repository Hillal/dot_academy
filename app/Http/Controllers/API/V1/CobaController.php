<?php


/**
* 
*/
namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class CobaController extends Controller
{
	// private function apiResponseBuilder($code,$data)
	// {
	// 	if ($code==200) {
	// 		$response['status'] = 200;
	// 		$response['data'] = $data;
	// 	}else{
	// 		$response['status'] = 500;
	// 		$response['data'] = $data;
	// 	}

	// 	return response()->json($response,$code);
	// }

	public function method_get()
	{
		$data = [
			'nama' => 'Hillal',
			'umur' => '18',
			'kota' => 'Tuban',
			'sekolah' => [
				'SDN Sukolilo II Tuban',
				'SMPN 1 Tuban',
				'SMK Telkom Malang'
			]	

		];

		return apiResponseBuilder('200',$data);
	
	}

	public function method_post(Request $request)
	{
		$data =$request->all();
		if($request->type == 'siswa'){
			$data['message'] = "Selamat Datang Siswa";
		}else{
			$data['message'] = "Selamat Datang Guru";
		}

		return apiResponseBuilder('200',$data);

	}

	public function trycatch(Request $request)
	{
		try{
			$data =$request->all();

			if(($request->type != 'siswa') and ($request->type != 'guru'))
				throw new Exception("type tidak diperbolehkan",1);
			if(!isset($request->nama)) 
				throw new Exception("Nama Harus Diisi", 1);
				
			// return $this->apiResponseBuilder('200',$data);
			$response =$data;
			$code = 200;
		}catch(Exception $e){
			$code = 500;
			$response = $e->getMessage();
			// return $this->apiResponseBuilder('500',$e->getMessage());
		}
		return apiResponseBuilder('200',$response);
	}
}

?>