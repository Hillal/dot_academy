<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Validator;
use App\User;

class RegisterController extends Controller
{
    public function register(Request $request)
    {

    	 $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        if ($validator->fails()) {
            return apiResponseValidationFails('Validation error messages!', $validator->errors()->all());
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);

        $success['user'] = $user;
        $success['token'] = $user->createToken('myApp')->accessToken;

        return apiResponseSuccess('Register success!', $success, 200);
    } 
 }
    	// try{
     //        $this->validate($request,[
     //            'name'=>'required',
     //            'email'=>'required',
     //            'password'=>'password',
     //        ]);

     //        $dataRegister = new Users;
            
            
     //        $dataRegister->name = $request->name;
     //        $dataRegister->email = $request->email;
     //        $dataRegister->password = $request->password;
            
     //        $dataRegister->save();
     //        $response=$dataRegister;
     //        $code=200;

     //    }catch(Exception $e){
     //        if($e instanceof ValidationException){
     //            $code = 400;
     //            $response = 'tidak ada data';
     //        }else{
     //            $code = 500;
     //            $response = $e->getMessage();  
     //        } 
            
     //    }
     //    return apiResponseBuilder($code,$response);
    	// return response()->json(['test1'=>'ok']);
    

