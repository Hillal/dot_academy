<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Show login form
     */
    public function showLogin()
    {
    	if (Auth::user()) {
    		return redirect("/product");
    	} else {
    		return view("auth.login");
    	}
    }

    /**
     * Do login process
     */
    public function doLogin(Request $request)
    {
    	// Get user credential (email, password)
    	$credential = $request->except("_token");

    	// Attempt to login
    	if (Auth::attempt($credential)) {
    		return redirect("/product");
    	} else {
    		return redirect()->back();
    	}
    }

    /**
     * Logout user
     */
    public function logout()
    {
    	Auth::logout();

    	// redirect using login route name (alias)
    	return redirect()->route("login");
    }
}
