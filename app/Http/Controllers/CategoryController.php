<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Category;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Category = Category::query();
        //search
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $Category->where(
                "name_category", "like", "%" . request()->query("search") . "%"
            );
        }

        // Query pagination
        $pagination = 5;
        $Category = $Category->orderBy('created_at', 'desc')
        ->paginate($pagination);

        // Handle perpindahan page
        $number = 1; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

        return view('category.index', compact('Category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

        $Category = new Category;
        $Category->product_count=0;

   		$Category->name_category = $request->name_category;
        $Category->product_count = 0;
        // $Category->category_id = $request->category_id;

   		$Category->save();

   		if($Category){
   			Session::flash('message','Berhasil tambah kategori');
   		}
   		// return redirect()->back();
        return redirect()->route("category.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Find category by id
        $Category = Category::with("products")->find($id);
        return view("category.detail", compact('Category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
   {
        $Category= Category::find($id);

        return view('category.edit', compact('Category'));
   }

   public function update(Request $request, $id)
   {
        $Category = Category::find($id);
        $Category->name_category = $request->name_category;

        $Category->save();

        if($Category){
            Session::flash('message','Berhasil update');
        }

        return redirect()->back();
   }

    public function delete($id)
    {

        $Category = Category::withTrashed()->find($id);
        if (!$Category->trashed()) {
            $Category->delete();
        }else{
            $Category->forceDelete();
        }
        return redirect()->back()->with(['message'=> 'Successfully deleted!!']);
    //     $Category = Category::find($id);

    //     $Category->delete();
    //     if($Category) {
    //         Session::flash('message','Berhasil menghapus');
    //     }

    //     return redirect()->back();
    }
    

}
