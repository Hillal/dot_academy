<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Products;
use App\Category;
use Session;

class ProductController extends Controller
{
   public function index()
   {

      //search
      $dataProduct = Products::query();
      if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataProduct->where(
                "nama", "like", "%" . request()->query("search") . "%"
            );
        }


      // Query pagination
        $pagination = 5;
        $dataProduct = $dataProduct->orderBy('created_at', 'desc')
        ->paginate($pagination);

        // Handle perpindahan page
        $number = 1; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }

        $Category = Category::select(['id', 'name_category'])->orderBy('name_category')->get();
   	 return view('product.index',compact('dataProduct', 'Category'));
   }

   public function store(Request $request)
   {

   		$this->validate($request, [
   			'nama' => 'required | min: 3',
   			'category_id' => 'required',
   			'unit_price' => 'required | numeric',
        'image' => 'required |image|mimes:jpg,jpeg,png,gif',
   		]);

      try {
           DB::beginTransaction();
           //query memnambah product_cout di category
           Category::where('id', $request->category_id)->increment('product_count');

           //upload file mengambil nama file
           $imageName = time().'.'.request()->image->getClientOriginalExtension();
           //menaruh di folder public/images
           request()->image->move(public_path('images'), $imageName);
           //query product
           $dataProduct = new Products;
           $dataProduct->nama = $request->nama;
           $dataProduct->category_id = $request->category_id;
           $dataProduct->unit_price = $request->unit_price;
           $dataProduct->image = $imageName;
           $dataProduct->save();
           DB::commit();
           Session::flash('message','Data berhasil disimpan');
           return redirect()->back();

       } catch (Exception $e) {
           DB::rollBack();
           Session::flash('message','Data tidak berhasil disimpan');
           return redirect()->back();

       }
   		// $dataProduct = new Products;

   		// $dataProduct->nama = $request->nama;
   		// $dataProduct->category_id = $request->category_id;
   		// $dataProduct->unit_price = $request->unit_price;

   		// $dataProduct->save();

   		// if($dataProduct){
   		// 	Session::flash('message','Berhasil tambah produk');
   		// }
   		// return redirect()->back();
   }

   public function edit($id)
   {
   		$dataProduct = Products::find($id);

      $Category = Category::select(['id', 'name_category'])->orderBy('name_category')->get();

   		return view('product.edit', compact('dataProduct', 'Category'));
   }

   public function update(Request $request, $id)
   {
   		$dataProduct = Products::find($id);

      if ($request->image) {

             // mengambil nama imagenya

             $imageName = time().'.'.request()->image->getClientOriginalExtension();

             // menaruh image pada folder public/assets/images/products

             request()->image->move(public_path('/images'), $imageName);               

            $dataProduct->image = $imageName;

         }
   		$dataProduct->nama = $request->nama;
   		$dataProduct->category_id = $request->category_id;
   		$dataProduct->unit_price = $request->unit_price;



   		$dataProduct->save();

   		if($dataProduct){
   			Session::flash('message','Berhasil update');
   		}

   		return redirect()->back();
   }

   public function detail($id)
	{
		$dataProduct = Products::find($id);

		return view('product.detail', compact('dataProduct'));
	}

	public function delete($id)
	{
		$dataProduct = Products::find($id);

		$dataProduct->delete();
		if($dataProduct) {
			Session::flash('message','Berhasil menghapus');
		}

		return redirect()->back();
	}

  public function nama()
  {
    echo "selamat datang";
  }
}
