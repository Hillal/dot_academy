<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use Session;

class CustomersController extends Controller
{
    public function index()
   {

   	$dataCustomers = Customers::query();
      //search
      if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $dataCustomers->where(
                "name", "like", "%" . request()->query("search") . "%"
            );
        }

      // Query pagination
        $pagination = 5;
        $dataCustomers = $dataCustomers->orderBy('created_at', 'desc')
        ->paginate($pagination);

        // Handle perpindahan page
        $number = 1; // Default page

        if (request()->has('page') && request()->get('page') > 1) {
            $number += (request()->get('page') - 1) * $pagination;
        }
   	 return view('customers.index',compact('dataCustomers'));
   }

   public function tambah(Request $request)
   {
   		$this->validate($request, [
   			'name' => 'required | min: 3',
   			'email' => 'required | min: 3',
   			'password' => 'required',
   			'phone_number' => 'required | numeric',
   			'address' => 'required',
   		]);

   		$dataCustomers = new Customers;

      $dataCustomers->first_name = $request->first_name;
   		$dataCustomers->name = $request->name;
   		$dataCustomers->email = $request->email;
   		$dataCustomers->password = $request->password;
   		$dataCustomers->phone_number = $request->phone_number;
   		$dataCustomers->address = $request->address;

   		$dataCustomers->save();

   		if($dataCustomers){
   			Session::flash('message','Berhasil tambah pelanggan');
   		}
   		return redirect()->back();
   }

   public function edit($id)
   {
   		$dataCustomers= Customers::find($id);

   		return view('customers.edit', compact('dataCustomers'));
   }

   public function update(Request $request, $id)
   {
   		$dataCustomers = Customers::find($id);
      $dataCustomers->first_name = $request->first_name;
   		$dataCustomers->name = $request->name;
   		$dataCustomers->email = $request->password;
   		$dataCustomers->password = $request->password;
   		$dataCustomers->phone_number = $request->phone_number;
   		$dataCustomers->address = $request->address;

   		$dataCustomers->save();

   		if($dataCustomers){
   			Session::flash('message','Berhasil update');
   		}

   		return redirect()->back();
   }

   public function detail($id)
	{
		$dataCustomers = Customers::find($id);

		return view('customers.detail', compact('dataCustomers'));
	}

   public function delete($id)
	{
		$dataCustomers = Customers::find($id);

		$dataCustomers->delete();
		if($dataCustomers) {
			Session::flash('message','Berhasil menghapus');
		}

		return redirect()->back();
	}

}