<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $table = "category";

    protected $fillable = ["name_category"];

    use SoftDeletes;
    /**
     * Relation to product, has many
     */
    public function products()
    {
    	/**
    	 * category_id => foreign key dari tabel product
    	 * id => local key
    	 */
    	return $this->hasMany(Products::class, 'category_id', 'id')->withTrashed();
    }

    public function delete()
    {
        $this->products()->delete();
        return parent::delete();
    }

}
